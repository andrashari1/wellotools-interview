import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import UserListPage from '../views/UserListPage.vue'
import UserPage from '../views/UserPage.vue'

const routes = [
  { path: '/', component: Home },
  { path: '/users', component: UserListPage },
  { path: '/user/:id', component: UserPage }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
