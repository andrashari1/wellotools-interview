import faker from "faker";
import fs from "fs";

(async () => {
  const users = [];
  for (let i = 0; i < 20; i++) {
    users.push({
      uuid: faker.random.uuid(),
      email: faker.internet.email(),
      name: faker.name.findName(),
      profileImage: faker.image.image()
    });
  }

  await fs.writeFile(
    "./src/faker/users.json",
    JSON.stringify(users),
    { flag: "w" },
    function(err) {
      if (err) throw err;
      console.log("It's saved!");
    }
  );
})();
