import { createStore } from 'vuex'
import users from '../faker/users.json'

export default createStore({
  state: {
    users: users,
    filters: {
      name: '',
      email: ''
    },
    currentSort: 'none',
    currentSortDir: 'asc'
  },
  computed: {},
  mutations: {
    updateFilters (state, data) {
      console.log(data, 'filters')
      state.filters = data
    },
    updateSorter (state, data) {
      console.log(data, 'sorters')
      const { currentSort, currentSortDir } = data
      state.currentSort = currentSort
      state.currentSortDir = currentSortDir
    },
    resetFilters (state) {
      Object.keys(state.filters).forEach(function (key) {
        state.filters[key] = ''
      })
    },
    resetSorter (state) {
      state.currentSort = null
      state.currentSortDir = null
    }
  },
  actions: {
    updateFilters ({ commit }, data) {
      commit('updateFilters', data)
    },
    resetFilters ({ commit }) {
      commit('resetFilters')
    }
  },
  modules: {}
})
